function getSmallestMultiple() {
  let smallestMultiple = 2520;

  for(let i = 11; i <= 20; i++) {
    if(smallestMultiple%i != 0) {
      i = 10;
      smallestMultiple++;
    }
  }
  return smallestMultiple;
}

module.exports = getSmallestMultiple;